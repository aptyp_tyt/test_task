<?php

get_header();
?>
	<div class="container">
		<h1> Registration Complete </h1>
		<h3>Thanks for registration! You have recieve message with link for manage your profile</h3>

		<a href="<?php echo get_home_url() ?>" class="form-group btn btn-default">Go back</a>
	</div>

<?php
get_footer();

wp_die();
