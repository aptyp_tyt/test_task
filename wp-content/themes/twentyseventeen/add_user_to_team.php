<?php get_header();

	$user_name = ($data['user_name']) ? $data['user_name'] : '';
	$user_surname = ($data['user_surname']) ? $data['user_surname'] : '';
	$user_mail = ($data['user_mail']) ? $data['user_mail'] : '';
	$user_phone = ($data['user_phone']) ? $data['user_phone'] : '';
	$expertise = ($data['expertise']) ? $data['expertise'] : '';

?>
<div class="row">
	<div class="container">
		<?php if(!empty($data['errors'])): ?>
			<div class="errors bg-danger mx-2 my-2 px-2 py-2 text-light"><?php echo $data['errors']; ?></div>
		<?php endif; ?>
		<form method="post" action="<?php echo admin_url('admin-ajax.php') ?>">
			<input type="hidden" name="action" value="add_user_to_team">
			<input type="hidden" name="tid" value="<?php echo $data['tid'] ?>">

		  <div class="form-group row">
		    <label for="staticTeam" class="col-sm-2 col-form-label">Team</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control-plaintext" id="staticTeam" value="<?php echo Team::find($data['tid'])->name; ?>">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputName" placeholder="Name" name="user_name" value="<?php echo $user_name; ?>">
		    </div>
		  </div>

		    <div class="form-group row">
			    <label for="inputSurname" class="col-sm-2 col-form-label">Surname</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="inputSurname" placeholder="Surname" name="user_surname" value="<?php echo $user_surname; ?>">
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="inputMail" class="col-sm-2 col-form-label">E-Mail</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" id="inputMail" placeholder="example@domain.com" name="user_mail" value="<?php echo $user_mail; ?>">
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="inputPhone" placeholder="Phone" name="user_phone" value="<?php echo $user_phone; ?>">
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="areaExpertise" class="col-sm-2 col-form-label">Your background and expertise:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="areaExpertise" placeholder="Expertise" name="expertise"><?php echo $expertise; ?></textarea>
			    </div>
			</div>

			<button class="form-group btn btn-default">Register</button>
			<a href="<?php echo get_home_url() ?>" class="form-group btn btn-default">Go back</a>

		</form>
	</div>
</div>


<?php
get_footer();
wp_die();