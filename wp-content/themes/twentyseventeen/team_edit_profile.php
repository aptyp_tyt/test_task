<?php
	get_header();

	global $team_hash;
	$team_hash = $data['team']->hash;

	$members_in_team = members_by_status($data['team']->id, 'IN_TEAM');
	$members_rejected = members_by_status($data['team']->id, 'REJECTED');
	$members_undef = members_by_status($data['team']->id);

	$in_team_count = (is_array($members_in_team) && !empty($members_in_team)) ? count($members_in_team) : 0;
	$rejected_count = (is_array($members_rejected) && !empty($members_rejected)) ? count($members_rejected) : 0;
	$undef_count = (is_array($members_undef) && !empty($members_undef)) ? count($members_undef) : 0;
?>
<div class="container">
		<?php if(!empty($data['errors'])): ?>
			<div class="errors bg-danger mx-2 my-2 px-2 py-2 text-light" id="error-alert"><?php echo $data['errors']; ?></div>
		<?php endif; ?>
		<?php if(!empty($data['success'])): ?>
			<div class="errors bg-success mx-2 my-2 px-2 py-2 text-light" id="#success-alert"><?php echo $data['success']; ?></div>
		<?php endif; ?>
	<div class="nav nav-pills mb-3" id="tab" role="tablist" aria-orientation="vertical">
		<a class="nav-link active" id="my-team-tab" data-toggle="pill" href="#my-team" role="tab" aria-controls="my-team" aria-selected="true">My team</a>
		<a class="nav-link" id="requests-tab" data-toggle="pill" href="#requests" role="tab" aria-controls="requests" aria-selected="false">Mebers requests <span>(<?php echo $undef_count; ?>)</span></a>
		<a class="nav-link" id="confirmed-tab" data-toggle="pill" href="#confirmed" role="tab" aria-controls="confirmed" aria-selected="false">Confirmed members <span>(<?php echo $in_team_count; ?>)</span></a>
		<a class="nav-link" id="rejected-tab" data-toggle="pill" href="#rejected" role="tab" aria-controls="rejected" aria-selected="false">Rejected members<span>(<?php echo $rejected_count; ?>)</span></a>
		<a class="nav-link" id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
	</div>

	<div class="tab-content" id="tabContent">
		<div class="tab-pane fade show active" id="my-team" role="tabpanel" aria-labelledby="my-team-tab"><h4>My team</h4>
			<?php
				load_template_with_data('members/table_head');
				if(!empty($members_in_team))
					if (is_array($members_in_team))
						foreach($members_in_team as $member){
							load_template_with_data('members/in_team', $member);
						}
					else
						load_template_with_data('members/in_team', $members_in_team);

				load_template_with_data('members/table_foot');
			?>
		</div>

		<div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="requests-tab"><h4>Members requests</h4>
			<?php
				load_template_with_data('members/table_head');
				if(!empty($members_undef))
					if (is_array($members_undef))
						foreach($members_undef as $member){
							load_template_with_data('members/undef', $member);
						}
					else
						load_template_with_data('members/undef', $members_undef);

				load_template_with_data('members/table_foot');
			?>
		</div>

		<div class="tab-pane fade" id="confirmed" role="tabpanel" aria-labelledby="confirmed-tab"><h4>Confirmed members</h4>
			<?php
				load_template_with_data('members/table_head');
				if(!empty($members_in_team))
					if (is_array($members_in_team))
						foreach($members_in_team as $member){
							load_template_with_data('members/confirmed', $member);
						}
					else
						load_template_with_data('members/confirmed', $members_in_team);

				load_template_with_data('members/table_foot');

			?>
		</div>

		<div class="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="rejected-tab"><h4>Rejected members</h4>
			<?php
				load_template_with_data('members/table_head');
				if(!empty($members_rejected))
					if (is_array($members_rejected))
						foreach($members_rejected as $member){
							load_template_with_data('members/rejected', $member);
						}
					else
						load_template_with_data('members/rejected', $members_rejected);

				load_template_with_data('members/table_foot');

			?>
		</div>

		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><h4>Profile</h4>
			<?php 
				$team_name = ($data['team']->name) ? $data['team']->name : '';
				$description = ($data['team']->description) ? $data['team']->description : '';
				$team_mail = ($data['team']->email) ? $data['team']->email : '';
			?>
		<form method="post" action="<?php echo admin_url('admin-ajax.php') ?>">
			<input type="hidden" name="action" value="utd">

		  <div class="form-group row">
		    <label for="inputName" class="col-sm-2 col-form-label">Team name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputName" placeholder="Name" name="team_name" value="<?php echo $team_name;?>">
		    </div>
		  </div>

		  <div class="form-group row">
			    <label for="areaExpertise" class="col-sm-2 col-form-label">Team description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="areaExpertise" placeholder="Description here" name="description"><?php echo $description;?></textarea>
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="inputMail" class="col-sm-2 col-form-label">Notification E-Mail</label>
			    <div class="col-sm-10">
			      <input type="email" readonly class="form-control" id="inputMail" placeholder="example@domain.com" name="team_mail" value="<?php echo $team_mail;?>">
			    </div>
			</div>

			<button class="form-group btn btn-default">Update information</button>

		</form>

		</div>
	</div>
</div>

<?php get_footer();	?>
<script>
	let int = window.setTimeout(function(){
		$("#error-alert").hide()
		$("#success-alert").hide()
	}, 3000);

</script>

<?php wp_die(); ?>
