<?php
	get_header();

	global $user_hash;
	$user_hash = $data['user']->hash;

	$has_team = teams_by_status($data['user']->id, 'IN_TEAM')[0];
	$rejections = teams_by_status($data['user']->id, 'REJECTED');
	$wait_for_request = teams_by_status($data['user']->id)[0];
	$not_requested = not_requested_teams($data['user']->id);

	$rejected_count = (is_array($rejections) && !empty($rejections)) ? count($rejections) : 0;
?>
<div class="container">
		<?php if(!empty($data['errors'])): ?>
			<div class="errors bg-danger mx-2 my-2 px-2 py-2 text-light" id="error-alert"><?php echo $data['errors']; ?></div>
		<?php endif; ?>
		<?php if(!empty($data['success'])): ?>
			<div class="errors bg-success mx-2 my-2 px-2 py-2 text-light" id="#success-alert"><?php echo $data['success']; ?></div>
		<?php endif; ?>
	<div class="nav nav-pills mb-3" id="tab" role="tablist" aria-orientation="vertical">
		<a class="nav-link active" id="my-team-tab" data-toggle="pill" href="#my-team" role="tab" aria-controls="my-team" aria-selected="true">My Info</a>
		<a class="nav-link" id="rejected-tab" data-toggle="pill" href="#rejected" role="tab" aria-controls="rejected" aria-selected="false">Teams Rejections<span>(<?php echo $rejected_count; ?>)</span></a>
		<a class="nav-link" id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
	</div>

	<div class="tab-content" id="tabContent">
		<div class="tab-pane fade show active" id="my-team" role="tabpanel" aria-labelledby="my-team-tab"><h4>My Information</h4>
			<div class="form-group row">
			    <label class="col-sm-2 col-form-label">Name: </label>
			    <div class="col-sm-10">
			      <div class="form-control"><?php echo $data['user']->name; ?></div>
			    </div>
			</div>
			<div class="form-group row">
			    <label class="col-sm-2 col-form-label">Surname: </label>
			    <div class="col-sm-10">
			      <div class="form-control"><?php echo $data['user']->surname; ?></div>
			    </div>
			</div>

			<div class="form-group row">
			    <label class="col-sm-2 col-form-label">Phone: </label>
			    <div class="col-sm-10">
			      <div class="form-control"><?php echo $data['user']->phone; ?></div>
			    </div>
			</div>

			<div class="form-group row">
			    <label class="col-sm-2 col-form-label">Expertise: </label>
			    <div class="col-sm-10">
			      <div class="form-control"><?php echo nl2br($data['user']->expertise); ?></div>
			    </div>
			</div>
			<h4> Teams: </h4>
			<?php if(!empty($has_team)) :?>
				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Team name: </label>
				    <div class="col-sm-10">
				      <div class="form-control"><?php echo $has_team->name; ?></div>
				    </div>
				</div>

				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Team description: </label>
				    <div class="col-sm-10">
				      <div class="form-control"><?php echo nl2br($has_team->description); ?></div>
				    </div>
				</div>

				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Status: </label>
				    <div class="col-sm-10">
				      <div class="form-control text-success">ACCEPTED</div>
				    </div>
				</div>
				<hr>				
			<?php endif; ?>


			<?php if(!empty($wait_for_request)) :?>
				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Team name: </label>
				    <div class="col-sm-10">
				      <div class="form-control"><?php echo $wait_for_request->name; ?></div>
				    </div>
				</div>

				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Team description: </label>
				    <div class="col-sm-10">
				      <div class="form-control"><?php echo $wait_for_request->description; ?></div>
				    </div>
				</div>

				<div class="form-group row">
				    <label class="col-sm-2 col-form-label">Status: </label>
				    <div class="col-sm-10">
				      <div class="form-control text-warning">WAIT FOR REQUEST</div>
				    </div>
				</div>	
				<a href="<?php echo admin_url('admin-ajax.php').'?action=delreq&uid='.$user_hash.'&tid='.$wait_for_request->id; ?>" class="btn btn-danger">Cancel Request</a>
				<hr>
			<?php endif; ?>

			<?php if(empty($wait_for_request) && empty($has_team)): ?>
				<h4 class="text-danger"> Team what you send request was reject you </h4>
					<?php if(!empty($not_requested)): ?>
				    	<a data-toggle="collapse" href="#teams" role="button" aria-expanded="false" aria-controls="teams" class="btn btn-default">Try another team</a>
				    	<div class="collapse" id="teams">
				    		<h1 class="text-center">Select team for entering</h1>
				    		<div class="container">
								<div class="flex-row row">
							    	<?php
							    		if (is_array($not_requested))
								    		foreach ($not_requested as $team) {
								    			load_template_with_data('teams/another_team', $team);
								    		}
								    	else{
								    		load_template_with_data('teams/another_team', $not_requested);
								    	}
								    ?>
								</div>
							</div>
						</div>
					<?php else: ?>
					 	<h4> No more teams found. Please check it later. </h4>
				    <?php endif; ?>
				</div>
			<?php endif; ?>

		<div class="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="rejected-tab"><h4>Teams rejections</h4>
			<?php
				load_template_with_data('teams/table_head');
				if(!empty($rejections))
					if (is_array($rejections))
						foreach($rejections as $rejection){
							load_template_with_data('teams/rejected', $rejection);
						}
					else
						load_template_with_data('teams/rejected', $rejection);

				load_template_with_data('teams/table_foot');

			?>
		</div>

		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><h4>Profile</h4>
			<?php 
				$user_name = ($data['user']->name) ? $data['user']->name : '';
				$user_surname = ($data['user']->surname) ? $data['user']->surname : '';
				$user_mail = ($data['user']->email) ? $data['user']->email : '';
				$user_phone = ($data['user']->phone) ? $data['user']->phone : '';
				$expertise = ($data['user']->expertise) ? $data['user']->expertise : '';
			?>
		<form method="post" action="<?php echo admin_url('admin-ajax.php') ?>">
			<input type="hidden" name="action" value="usi">
			<input type="hidden" name="uid" value="<?php echo $data['user']->hash; ?>">

		  <div class="form-group row">
		    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputName" placeholder="Name" name="user_name" value="<?php echo $user_name; ?>">
		    </div>
		  </div>

		    <div class="form-group row">
			    <label for="inputSurname" class="col-sm-2 col-form-label">Surname</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="inputSurname" placeholder="Surname" name="user_surname" value="<?php echo $user_surname; ?>">
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="inputPhone" placeholder="Phone" name="user_phone" value="<?php echo $user_phone; ?>">
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="areaExpertise" class="col-sm-2 col-form-label">Your background and expertise:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="areaExpertise" placeholder="Expertise" name="expertise"><?php echo $expertise; ?></textarea>
			    </div>
			</div>

			<div class="form-group row">
			    <label for="inputMail" class="col-sm-2 col-form-label">E-Mail</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" id="inputMail" readonly placeholder="example@domain.com" name="user_mail" value="<?php echo $user_mail; ?>">
			    </div>
			</div>

			<button class="form-group btn btn-default">Update information</button>
		</form>

		</div>
	</div>
</div>

<?php get_footer();	?>
<script>
	let int = window.setTimeout(function(){
		$("#error-alert").hide()
		$("#success-alert").hide()
	}, 3000);

</script>

<?php wp_die(); ?>
