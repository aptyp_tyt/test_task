<?php global $team_hash; ?>
<tr>
  <td><?php echo $data->name; ?></td>
  <td><?php echo $data->surname; ?></td>
  <td><?php echo $data->phone; ?></td>
  <td><?php echo $data->expertise; ?></td>
  <th scope="row" class="d-flex justify-content-around"><a href="<?php echo admin_url('admin-ajax.php').'?action=reject&uid='.$team_hash.'&member='.$data->id; ?>" class="btn btn-danger">Delete from team</a></th>
</tr>