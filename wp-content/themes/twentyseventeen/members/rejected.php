<?php global $team_hash; ?>

<tr>
  <td><?php echo $data->name; ?></td>
  <td><?php echo $data->surname; ?></td>
  <td><?php echo $data->phone; ?></td>
  <td><?php echo $data->expertise; ?></td>
  <th scope="row" class="d-flex justify-content-around"><a href="<?php echo admin_url('admin-ajax.php').'?action=accept&uid='.$team_hash.'&member='.$data->id; ?>" class="btn btn-default">Try to accept</a></th>
</tr>