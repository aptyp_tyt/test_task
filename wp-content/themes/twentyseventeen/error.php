<?php get_header(); ?>

	<div class="container">
		<h1> Error </h1>
		<h3><?php echo $data; ?></h3>

		<a href="<?php echo get_home_url() ?>" class="form-group btn btn-default">Go back</a>
	</div>

<?php
get_footer();

wp_die();
