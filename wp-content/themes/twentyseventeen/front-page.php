<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
	$num_sections = apply_filters( 'twentyseventeen_front_page_sections', 4 );
	global $twentyseventeencounter;

	for ( $i = 1; $i < $num_sections+1; $i++ ) {
		$twentyseventeencounter = $i;
		twentyseventeen_front_page_section( null, $i );
	}
get_footer();

?>


