<?php global $user_hash; ?>

<div class="col-md-6 col-xs-12 col-sm-6 col-lg-4 tale">
	<div class="thumbnail">
		<div class="caption">
			<h3><?php echo $data->name; ?></h3>
			<p class="flex-text text-muted"><?php echo $data->description; ?></p>
			<p><a class="btn btn-default" href="<?php echo admin_url('admin-ajax.php').'?action=try_another&uid='.$user_hash.'&tid='.$data->id; ?>">Try this!</a></p>
		</div>
	</div>
</div>