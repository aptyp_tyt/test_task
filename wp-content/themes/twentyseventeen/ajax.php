<?php

function is_mail_unique($mail){
    return (
        (
            empty(User::find_by_email($mail)) && empty(Team::find_by_email($mail))
        )
    );
}

function add_js_variables(){
    $vars = array(
        	'ajax_url' => admin_url('admin-ajax.php')
    	);
    	
    $json = json_encode($vars);

    echo<<<EOF
        <script type='text/javascript'>
            window.wp_data = $json
        </script>
EOF;
}

add_action('wp_head','add_js_variables');


function add_ajax($action = false, $callback = false, $callback_nopriv = false){

	if ($action === false || $callback === false) return;
	$callback_nopriv = ($callback_nopriv) ? $callback_nopriv : $callback;

	add_action("wp_ajax_$action", $callback);
	add_action("wp_ajax_nopriv_$action", $callback_nopriv);
}

function add_to_team_callback(){
    $tid = intval($_GET['tid']);
    if ($tid != 0){
        load_template_with_data('add_user_to_team', array('tid' => $tid));
    }
    else {
        $data = "Error not correct data!";
        load_template_with_data('error', $data);
    }

}
add_ajax('add_to_team', 'add_to_team_callback');


function register_new_team_callback(){
    load_template_with_data('create_team');
}
add_ajax('register_new_team', 'register_new_team_callback');


function add_user_to_team_callback(){

    $errors = '';

    $user = new User();

    $tid = intval($_REQUEST['tid']);
    $email = sanitize_email($_REQUEST['user_mail']);

    if(empty(Team::find($tid))){
        $errors = "Team does not exist";
        $data = array_merge($_REQUEST, array('errors' => $errors));

        load_template_with_data('add_user_to_team', $data);
        return;
    }

    $user->name = sanitize_text_field($_REQUEST['user_name']);
    $user->surname = sanitize_text_field($_REQUEST['user_surname']);
    $user->email = $email;
    $user->phone = sanitize_text_field($_REQUEST['user_phone']);
    $user->expertise = sanitize_text_field($_REQUEST['expertise']);


    if ($user->is_invalid()){
        $errors .= (!empty($user->errors->on('name'))) ? $user->errors->on('name').'<br />' : '';
        $errors .= (!empty($user->errors->on('surname'))) ? $user->errors->on('surname').'<br />' : '';
        $errors .= (!empty($user->errors->on('email'))) ? $user->errors->on('email').'<br />' : '';
        $errors .= (!empty($user->errors->on('phone'))) ? $user->errors->on('phone').'<br />' : '';
        $errors .= (!empty($user->errors->on('expertise'))) ? $user->errors->on('expertise').'<br />' : '';

        $data = array_merge($_REQUEST, array('errors' => $errors));

        load_template_with_data('add_user_to_team', $data);
        return;
    }

    if(!is_mail_unique($email)){
        $errors = 'You have already registered!';
        $data = array_merge($_REQUEST, array('errors' => $errors));
        load_template_with_data('add_user_to_team', $data);
        return;
    }


    $hash = substr(md5(date('Y-m-d H:i:s:ms').$user->name.$user->surname.$user->email), 0, 44);

    $user->hash = $hash;

    $user->save();

    $team_user = new TeamUser();

    $team_user->user_id = $user->id;
    $team_user->team_id = $tid;

    $team_user->save();
    do_action('register_complete');
}

add_ajax('add_user_to_team', 'add_user_to_team_callback');



function create_team_callback(){

    $errors = '';

    $email = sanitize_email($_REQUEST['team_mail']);


    $team = new Team();

    $team->name = sanitize_text_field($_REQUEST['team_name']);
    $team->description = sanitize_text_field($_REQUEST['description']);
    $team->email = sanitize_email($_REQUEST['team_mail']);



    if ($team->is_invalid()){
        $errors .= (!empty($team->errors->on('name'))) ? $team->errors->on('name').'<br />' : '';
        $errors .= (!empty($team->errors->on('description'))) ? $team->errors->on('description').'<br />' : '';
        $errors .= (!empty($team->errors->on('email'))) ? $team->errors->on('email').'<br />' : '';

        $data = array_merge($_REQUEST, array('errors' => $errors));

        load_template_with_data('create_team', $data);
        return;
    }

    if(!is_mail_unique($email)){
        $errors = 'You have already registered!';
        $data = array_merge($_REQUEST, array('errors' => $errors));
        load_template_with_data('create_team', $data);
        return;
    }

    if(!empty(Team::find_by_name($team->name))){
        $errors = 'This team name already registered!';
        $data = array_merge($_REQUEST, array('errors' => $errors));
        load_template_with_data('create_team', $data);
        return;
    }

    $hash = substr(md5(date('Y-m-d H:i:s:ms').$team->name.$team->email), 0, 44);

    $team->hash = $hash;

    $team->save();

    do_action('register_complete');
}

add_ajax('create_team', 'create_team_callback');

function edit_profile(){
    $hash = sanitize_text_field($_REQUEST['uid']);

    if (empty($hash)){
        profile_not_found();
        return;
    }

    $data = User::find_by_hash($hash);

    if(!empty($data)){
        load_template_with_data('user_edit_profile', array('user' => $data));
        return;
    }

    $data = Team::find_by_hash($hash);

    if(!empty($data)){
        load_template_with_data('team_edit_profile', array('team'=>$data));
        return;
    }
    profile_not_found();
}

add_ajax('edit', 'edit_profile');

function profile_not_found(){
    $data = "Profile not found!";
    load_template_with_data('error', $data);
}


function load_user_edit_profile($hash, $data = false){
    $data = ($data) ? $data : User::find_by_hash($hash);

    if(!empty($data)){
        load_template_with_data('user_edit_profile', $data);
        return;
    }
    profile_not_found();
}



function load_team_edit_profile($hash, $data = false){
    $data = ($data) ? $data : Team::find_by_hash($hash);

    if(!empty($data)){
        load_template_with_data('team_edit_profile', $data);
        return;
    }
    profile_not_found();
}


function try_accept_member(){
    $uid = intval($_REQUEST['member']);
    $hash = sanitize_text_field($_REQUEST['uid']);
    $error = '';
    $success = '';

    $t = TeamUser::find_by_user_id($uid);

    if ($uid == 0 || empty($hash) || empty($t)){
        $data = "You are young hacker, isn't it?";
        load_template_with_data('error', $data);
        return;
    }

    $data = array('team'=>Team::find_by_hash($hash));

    if (is_user_busy($uid)){
        $error = "User already taken another team. We'll so sorry!";
        $data['errors'] = $error;
        load_template_with_data('team_edit_profile', $data);
        return;
    }


    $t->status='IN_TEAM';

    if($t->save()) {
        $success = "User successfully added!";
        $data['success'] =  $success;
        load_team_edit_profile($hash, $data);
        return;
    }
    else{
        $error = "Something went wrong. We'll so sorry!";
        $data['errors'] = $error;
        load_template_with_data('team_edit_profile', $data);
        return; 
    }
}


add_ajax('accept','try_accept_member');


function reject_member(){
    $uid = intval($_REQUEST['member']);
    $hash = sanitize_text_field($_REQUEST['uid']);
    $error = '';
    $success = '';

    
    $data = array('team'=>Team::find_by_hash($hash));

    $conditions = array(
        'conditions' => array(
            'user_id = ? AND team_id = ?', $uid, $data['team']->id
        )
    );

    $t = TeamUser::find('first', $conditions);

    if ($uid == 0 || empty($hash) || empty($t)){
        $data = "You are young hacker, isn't it?";
        load_template_with_data('error', $data);
        return;
    }

    $t->status='REJECTED';


    if($t->save()) {
        $success = "You are successfully rejected user!";
        $data['success'] = $success;
        load_team_edit_profile($hash, $data);
        return;
    }
    else{
        $error = "Something went wrong. We'll so sorry!";
        $data['errors'] =$error;
        load_template_with_data('team_edit_profile', $data);
        return; 
    }
}

add_ajax('reject','reject_member');


function edit_team_profile(){
    $hash = sanitize_text_field($_REQUEST['uid']);
    $error = '';
    $success = '';

    $team = Team::find_by_hash($hash);

    if (empty($hash) || empty($team)){
        $data = "You are young hacker, isn't it?";
        load_template_with_data('error', $data);
        return;
    }

    $team->name = sanitize_text_field($_REQUEST['team_name']);
    $team->description = sanitize_text_field($_REQUEST['description']);

    $data = array('team'=>$team);

    if($team->save()) {
        $success = "You are successfully update profile data!";
        load_team_edit_profile('team_edit_profile', array('team'=>$team, 'success'=>$success));
        return;
    }
    else{
        $error = "Something went wrong. We'll so sorry!";
        load_template_with_data('team_edit_profile', array('team'=>$team, 'error'=>$error));
        return; 
    }
}

add_ajax('utd','edit_team_profile');


function try_another_team(){
    $hash = sanitize_text_field($_REQUEST['uid']);
    $tid = intval($_REQUEST['tid']);

    $user = User::find_by_hash($hash);

    if (empty($hash) || empty($user) || $tid == 0){
        $data = "You are young hacker, isn't it?";
        load_template_with_data('error', $data);
        return;
    }

    $team = Team::find($tid);

    if (empty($team)){
        $error = "Team not found!";
        load_template_with_data('user_edit_profile', array('user' => $user, 'errors'=>$error));
        return; 
    }

    $tu = TeamUser::find_by_team_id($team->id);

    if (!empty($tu)){
        $error = "You are already try this team!";
        load_template_with_data('user_edit_profile', array('user' => $user, 'errors'=>$error));
        return; 
    }

    $tu = new TeamUser();
    $tu->user_id = $user->id;
    $tu->team_id = $team->id;

    if($tu->save()){
        $success = "You are successfuly send request to team!";
        load_template_with_data('user_edit_profile', array('user' => $user, 'success'=>$success));
        return; 
    }
    else{
        $error = "Something went wrong! Try later.";
        load_template_with_data('user_edit_profile', array('user' => $user, 'errors'=>$error));
        return; 
    }
}

add_ajax('try_another','try_another_team');


function edit_user_profile(){
    $hash = sanitize_text_field($_REQUEST['uid']);
    $error = '';
    $success = '';

    $user = User::find_by_hash($hash);

    if(empty($user)){
        $errors = "User not found";
        load_template_with_data('user_edit_profile', array('user'=>$user, 'errors'=>$errors));
        return;
    }

    $user->name = sanitize_text_field($_REQUEST['user_name']);
    $user->surname = sanitize_text_field($_REQUEST['user_surname']);
    $user->phone = sanitize_text_field($_REQUEST['user_phone']);
    $user->expertise = sanitize_text_field($_REQUEST['expertise']);


    if ($user->is_invalid()){
        $errors .= (!empty($user->errors->on('name'))) ? $user->errors->on('name').'<br />' : '';
        $errors .= (!empty($user->errors->on('surname'))) ? $user->errors->on('surname').'<br />' : '';
        $errors .= (!empty($user->errors->on('email'))) ? $user->errors->on('email').'<br />' : '';
        $errors .= (!empty($user->errors->on('phone'))) ? $user->errors->on('phone').'<br />' : '';
        $errors .= (!empty($user->errors->on('expertise'))) ? $user->errors->on('expertise').'<br />' : '';

        load_template_with_data('user_edit_profile', array('user'=>$user, 'errors'=>$errors));
        return;
    }

    $user->save();

    load_template_with_data('user_edit_profile', array('user'=>$user));
}

add_ajax('usi','edit_user_profile');


function delete_request(){
    $hash = sanitize_text_field($_REQUEST['uid']);
    $tid = intval($_REQUEST['tid']);
    $error = '';
    $success = '';

    $user = User::find_by_hash($hash);

    if(empty($user)){
        $errors = "User not found";
        load_template_with_data('user_edit_profile', array('user'=>$user, 'errors'=>$errors));
        return;
    }

    if (empty($hash) || empty($user) || $tid == 0){
        $data = "You are young hacker, isn't it?";
        load_template_with_data('error', $data);
        return;
    }

    $conditions = array(
        'conditions' => array(
            'user_id = ? AND team_id = ?', $user->id, $tid
        )
    );

    $tu = TeamUser::find('first', $conditions);

    if(empty($tu)){
        $error = 'Request not found!';
        load_template_with_data('user_edit_profile', array('user'=>$user, 'errors'=> $error));
    }
    else{
        $tu->delete();
        $success = 'Request successfully canceled';
        load_template_with_data('user_edit_profile', array('user'=>$user, 'success'=>$success));
    }
}

add_ajax('delreq','delete_request');