<?php

	require_once ABSPATH.'activerecord/ActiveRecord.php';

	ActiveRecord\Config::initialize(function($cfg){
	  $cfg->set_model_directory(ABSPATH."Models");
	  $cfg->set_default_connection('mysql://test_task:123@localhost/test_task');
	});