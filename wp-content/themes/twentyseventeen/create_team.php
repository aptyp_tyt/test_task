<?php get_header();
	$team_name = ($data['team_name']) ? $data['team_name'] : '';
	$description = ($data['description']) ? $data['description'] : '';
	$team_mail = ($data['team_mail']) ? $data['team_mail'] : '';
?>
<div class="row">
	<div class="container">
		<?php if(!empty($data['errors'])): ?>
			<div class="errors bg-danger mx-2 my-2 px-2 py-2 text-light"><?php echo $data['errors']; ?></div>
		<?php endif; ?>
		<form method="post" action="<?php echo admin_url('admin-ajax.php') ?>">
			<input type="hidden" name="action" value="create_team">

		  <div class="form-group row">
		    <label for="inputName" class="col-sm-2 col-form-label">Team name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputName" placeholder="Name" name="team_name" value="<?php echo $team_name;?>">
		    </div>
		  </div>

		  <div class="form-group row">
			    <label for="areaExpertise" class="col-sm-2 col-form-label">Team description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="areaExpertise" placeholder="Description here" name="description"><?php echo $description;?></textarea>
			    </div>
			</div>

		    <div class="form-group row">
			    <label for="inputMail" class="col-sm-2 col-form-label">Notification E-Mail</label>
			    <div class="col-sm-10">
			      <input type="email" class="form-control" id="inputMail" placeholder="example@domain.com" name="team_mail" value="<?php echo $team_mail;?>">
			    </div>
			</div>

			<button class="form-group btn btn-default">Register Team</button>
			<a href="<?php echo get_home_url() ?>" class="form-group btn btn-default">Go back</a>

		</form>
	</div>
</div>


<?php
get_footer();
wp_die();