<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_task');

/** MySQL database username */
define('DB_USER', 'test_task');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Zd14m9+E7aV`q/>Y]%v!-SBJ]po$Oe*E^oV$Z)-S9{Xmq9:s#jMr(ctTG$`6OnpW');
define('SECURE_AUTH_KEY',  '<1Fz,)MP<`3 A#!-3Nt>~ {xX9C@sKrMrKV7?j#*GD-WxY/|LY|{M9&KLI{iX4e_');
define('LOGGED_IN_KEY',    'Xh8AI/t{:Qx+fL0wNdU&s<ve,vD~.BeIEZ]<h_n;4|17%7Rxm+?~l91DP}!<?00#');
define('NONCE_KEY',        '0=ixaB+{@A!oS-Z:,W$f*A?=uE80%-EpsI9SRht`XT eJz)s{ZYNMZVedA*KH0bE');
define('AUTH_SALT',        'u2O*)b}@?!Bggn!L=KO4l,@r{rj^Qvl^oQEcHP<WzsUPM*<4b?EYJGWoq3.LO~vH');
define('SECURE_AUTH_SALT', ';VWXKyi:5uy.WOEs4r[F2k%b^kT!YjQrLa+B-JjiL}iALDse6.qF/@Bd4pCuKL!Q');
define('LOGGED_IN_SALT',   '{m5|LVbNI[STHus+$_-:6rl<@v,?{V0NY}/{hcs+_f2]QvO{Ipo Yox:acR3ur=h');
define('NONCE_SALT',       '4yV},cB#PlaAnOWLq){ 8B=kvm8wI=}0p%9W1Abk1uZrfPx8`s`HIufcD]:r7%aF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'twpt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
