<?php

	class User extends ActiveRecord\Model{
		static $has_many = array(
			array('team_users')
		);

		static $validates_presence_of = array(
			array('name', 'message' => 'Name must be presence'),
			array('email', 'message' => 'E-Mail must be presence')
		);
	}