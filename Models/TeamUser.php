<?php

	class TeamUser extends ActiveRecord\Model{
		static $belongs_to = array(
			array('team'),
			array('user')
		);
	}